"""Test deprecatedby."""

import deprecated


def test_deprecation_docstring_with_no_original_docstring():
    """Test deprecated.by without a docstring."""
    def replacement_function():
        pass

    @deprecated.by(replacement_function)
    def deprecated_function():
        pass

    assert 'DEPRECATED' in deprecated_function.__doc__
    assert replacement_function.__qualname__ in deprecated_function.__doc__


def test_deprecation_docstring_with_original_docstring():
    """Test deprecated.by with a docstring."""
    def replacement_function():
        pass

    @deprecated.by(replacement_function)
    def deprecated_function():
        """deprecated_function"""

    assert 'DEPRECATED' in deprecated_function.__doc__
    assert """deprecated_function""" in deprecated_function.__doc__
