from setuptools import setup


def read(filename):
    with open(filename) as f:
        return f.read()


setup(
    name='deprecatedby',
    version='1.0.0',
    author='Andy Dirnberger',
    author_email='andy@dirnberger.me',
    url='https://deprecatedby.readthedocs.io',
    description='A decorator to mark one function as deprecated by another.',
    long_description=read('README.rst'),
    license='MIT',
    pymodules=['deprecated'],
    zip_safe=False,
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
