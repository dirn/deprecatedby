"""A decorator to mark one function as deprecated by another."""

from functools import wraps
from typing import Any, Callable, Optional
import warnings


def by(
        replacement: Callable,
        version_removed: Optional[str] = None,
) -> Callable:
    def wrapper(fn: Callable) -> Callable:
        @wraps(fn)
        def inner(*args: Any, **kwargs: Any) -> Any:
            message = (
                f'{fn.__qualname__} has been deprecated, '
                f'use {replacement.__qualname__} instead.'
            )

            warnings.warn(message, DeprecationWarning)

            return replacement(*args, **kwargs)

        if inner.__doc__ is None:
            inner.__doc__ = ''
        else:
            inner.__doc__ = f'\n\n{inner.__doc__}'

        message = ' '.join([
            f'{fn.__qualname__} will be removed in',
            f'version {version_removed}.' if version_removed else 'a future version.',
            f'Use {replacement.__qualname__} instead.',
        ])
        inner.__doc__ = f'DEPRECATED: {message}{inner.__doc__}'

        return inner
    return wrapper
