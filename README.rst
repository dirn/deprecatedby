############
deprecatedby
############

Installation
############

    $ pip3 install deprecatedby


Usage
#####

.. code::

    import deprecated

    @deprecated.by(replacement_version, '2.0')
    def deprecated_function():
        pass
